package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"errors"
)

type TGBot struct {
	key, chatID string
}

func (bot TGBot) Method(method string, params map[string]string) (ApiResponse, error) {
	jsonParams, err := json.Marshal(params)
	req, _ := http.NewRequest("POST", "https://api.telegram.org/bot" + bot.key + "/" + method, bytes.NewBuffer([]byte(jsonParams)))
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36")
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.Status != "200 OK" {
		return nil, errors.New("http status: " + resp.Status)
	}

	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		return nil, err1
	}

	var data map[string]interface{}
	err2 := json.Unmarshal([]byte(body), &data)
	if err2 != nil {
		return nil, err2
	}

	return data, nil
}

func (bot TGBot) SendMessage(text string) {
	params := make(map[string]string)
	params["chat_id"] = bot.chatID
	params["text"] = text
	if _, err := bot.Method("sendMessage", params); err != nil {
		panic(err.Error())
	}
}