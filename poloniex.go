package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"jsonparser"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"websocket"
)

type Poloniex struct {
	key, secret string
	book *Book
}

func (s Poloniex) Method(method string, params ApiParams) ([]byte, error) {
	payload := url.Values{}
	payload.Add("command", method)
	payload.Add("nonce", nonce())
	if params != nil {
		for key, value := range params {
			payload.Add(key, value)
		}
	}
	payloadSrt := payload.Encode()

	sign := hmacMessage("sha512", payloadSrt, s.secret)

	req, _ := http.NewRequest("POST", "https://poloniex.com/tradingApi", bytes.NewBuffer([]byte(payloadSrt)))
	req.Header.Set("Key", s.key)
	req.Header.Set("Sign", sign)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36")
	req.Header.Add("Content-Length", strconv.Itoa(len(payloadSrt)))

	client := &http.Client{}
	resp, err1 := client.Do(req)
	if err1 != nil {
		return nil, err1
	}
	defer resp.Body.Close()

	body, err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		return nil, err2
	}

	if errorStr, err := jsonparser.GetString(body, "error"); err == nil {
		return nil, errors.New(errorStr)
	}

	return body, nil
}

func (s Poloniex) Balance(symbol string) (float64, error) {
	resp, err1 := s.Method("returnCompleteBalances", nil)
	if err1 != nil {
		return 0, err1
	}

	balanceStr, err2 := jsonparser.GetString(resp, symbol, "available")
	if err2 != nil {
		return 0, err2
	}

	balance, err3 := strconv.ParseFloat(balanceStr, 64)
	if err3 != nil {
		return 0, err3
	}

	return balance, nil
}

func (s Poloniex) PlaceOrder(order MyOrder) error {
	pair := order.Pair[3:] + "_" + order.Pair[:3] // в poloniex пары перевернуты
	params := make(map[string]string)
	params["currencyPair"] = pair
	params["rate"] = fmt.Sprintf("%f", order.Price)
	params["amount"] = fmt.Sprintf("%f", order.Amount)

	resp, err := s.Method(order.Type, params)
	if err != nil {
		return err
	}

	if id, err := jsonparser.GetString(resp, "orderNumber"); err == nil {
		order.ID = id
	}
	s.book.AddMyOrder(order)

	return nil
}

func (s Poloniex) OpenOrdersByPair(pair string) ([]interface{}, error) {
	pair = pair[3:] + "_" + pair[:3] // в poloniex пары перевернуты
	params := make(map[string]string)
	params["currencyPair"] = pair

	resp, err := s.Method("returnOpenOrders", params)
	if err != nil {
		return nil, err
	}

	var orders []interface{}
	if err := json.Unmarshal(resp, &orders); err != nil {
		return nil, err
	}

	return orders, nil
}

func (s Poloniex) Pusher() {
	c, _, err := websocket.DefaultDialer.Dial("wss://api2.poloniex.com", nil)

	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	currencies := []string{"BTC_DOGE", "BTC_DASH", "BTC_LTC", "BTC_NXT", "BTC_XMR", "BTC_XRP", "BTC_ETH", "BTC_LSK", "BTC_ETC", "BTC_ZEC", "BTC_GNT", "BTC_BCH", "BTC_ZRX", "BTC_EOS", "USDT_BTC", "USDT_DOGE", "USDT_DASH", "USDT_LTC", "USDT_NXT", "USDT_XMR", "USDT_XRP", "USDT_ETH", "USDT_LSK", "USDT_ETC", "USDT_ZEC", "USDT_GNT", "USDT_BCH", "USDT_ZRX", "USDT_EOS", "XMR_DASH", "XMR_LTC", "XMR_NXT", "XMR_ZEC", "ETH_LSK", "ETH_ETC", "ETH_ZEC", "ETH_GNT", "ETH_BCH", "ETH_ZRX", "ETH_EOS"}
	//currencies := []string{"USDT_BTC"}
	pairs := map[int]string{}

	for _, v := range currencies {
		response := fmt.Sprintf(`{"command": "subscribe", "channel": "%s" }`, v)

		if err := c.WriteMessage(websocket.TextMessage, []byte(response)); err != nil {
			log.Println("send:", err)
		}
	}

	for {
		_, json, err := c.ReadMessage()

		if err != nil {
			log.Println("read:", err)
			return
		}

		if len(currencies) != len(pairs) { // waiting for filling all channels ID
			pair, pairErr := jsonparser.GetString(json, "[2]", "[0]", "[1]", "currencyPair")

			if pairErr != nil {
				continue
			}

			//re := regexp.MustCompile("(USD)T_([A-z]+)")
			re := regexp.MustCompile("([A-z]+)_([A-z]+)")
			pair = re.ReplaceAllString(pair, "$2$1")

			pair = strings.Replace(pair, "_", "", 1)

			chanId, _ := jsonparser.GetInt(json, "[0]")
			pairs[int(chanId)] = pair

			continue
		}
		chanId, _ := jsonparser.GetInt(json, "[0]")
		pair := pairs[int(chanId)]

		_, errBook := jsonparser.ArrayEach(json, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
			action, _ := jsonparser.GetString(value, "[0]")

			if action == "o" {
				typeBook, _ := jsonparser.GetInt(value, "[1]")
				p, _ := jsonparser.GetString(value, "[2]")
				a, _ := jsonparser.GetString(value, "[3]")
				price, _ := strconv.ParseFloat(p, 64)
				amount, _ := strconv.ParseFloat(a, 64)

				if typeBook == 1 { // bid
					s.book.AddBid(pair, price, amount)
				} else {
					s.book.AddAsk(pair, price, amount)
				}
			}
		}, "[2]")

		if errBook != nil {
			continue
		}

		//log.Printf("recv: %s", json)
	}
}

func (s Poloniex) GetBook() *Book {
	return s.book
}