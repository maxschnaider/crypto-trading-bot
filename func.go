package main

import (
	"bytes"
	"compress/flate"
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hash"
	"io/ioutil"
	"jsonparser"
	"log"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"time"
	"websocket"
)

func hmacMessage(algorithm string, message string, key string) string {
	var mac hash.Hash

	if algorithm == "sha512" {
		mac = hmac.New(sha512.New, []byte(key))
	} else if algorithm == "sha256" {
		mac = hmac.New(sha256.New, []byte(key))
	} else if algorithm == "sha384" {
		mac = hmac.New(sha512.New384, []byte(key))
	}

	mac.Write([]byte(message))

	return fmt.Sprintf("%x", mac.Sum(nil))
}

func nonce() string {
	return fmt.Sprintf("%d", time.Now().UnixNano())
}

func inflate(str string) []byte { // for decoding SignalR data
	data, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		panic(err.Error())
	} else {
		if inflate, err := ioutil.ReadAll(flate.NewReader(bytes.NewReader(data))); err != nil {
			panic(err.Error())
		} else {
			return inflate
		}
	}
}

func get(url string, r func(response []byte)) {
	client := &http.Client{Timeout: 2 * time.Second}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36")

	if resp, err := client.Do(req); err == nil {
		if body, err := ioutil.ReadAll(resp.Body); err == nil {
			r(body)
			defer resp.Body.Close()
		}
	} else {
		r(nil)
	}

}

func loadSettings(file string) (map[string]string, error) {
	m := make(map[string]string)
	ex, err := os.Executable()

	if err != nil {
		panic(err)
	}

	if b, err := ioutil.ReadFile(filepath.Dir(ex) + "/" + file); err == nil {
		err := jsonparser.ObjectEach(b, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
			m[string(key)] = string(value)
			return nil
		})

		if err != nil {
			panic(err)
		}

		return m, err
	} else {
		return nil, err
	}
}

func pusher(c *websocket.Conn) {
	ticker := time.NewTicker(500 * time.Microsecond)

	for {
		select {
		case message := <-clients[c].ch:
			if err := c.WriteMessage(1, message); err != nil {
				log.Println(err)
				return
			}
		case <-ticker.C:
			if err := c.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func reader(c *websocket.Conn) {
	defer func() {
		close(clients[c].ch) // close channel
		delete(clients, c)
		c.Close()
	}()

	for {
		if _, msg, err := c.ReadMessage(); err != nil {
			break
		} else {
			getAction(c, msg)
		}
	}
}

func sendToAll(message []byte) {
	for _, client := range clients {
		client.ch <- message
	}
}

func round(n float64) float64 {
	return math.Round(n*1000) / 1000
}

func getAction(c *websocket.Conn, message []byte) {
	if a, _, _, err := jsonparser.Get(message, "action"); err == nil {
		action := string(a)

		if action == "getPairs" {
			var book *Book

			if e, _, _, err := jsonparser.Get(message, "exchange"); err == nil {
				exchange := string(e)

				if exchange == "cexio" {
					book = exchanges[0].GetBook()
				} else if exchange == "bitfinex" {
					book = exchanges[1].GetBook()
				} else if exchange == "bittrex" {
					book = exchanges[2].GetBook()
				} else if exchange == "exmo" {
					book = exchanges[3].GetBook()
				} else if exchange == "poloniex" {
					book = exchanges[4].GetBook()
				}
			}

			r := ActionResponse{"getPairs", book.GetPairs()}

			if json, errJson := json.Marshal(r); errJson == nil {
				clients[c].ch <- json
			}
		} else if action == "subscribe" {
			exchange, _, _, _ := jsonparser.Get(message, "exchange")
			pair, _, _, _ := jsonparser.Get(message, "pair")

			clients[c].subscribed = true
			clients[c].exchange = string(exchange)
			clients[c].pair = string(pair)
		} else if action == "unsubscribe" {
			clients[c].subscribed = false
			clients[c].exchange = ""
			clients[c].pair = ""
		}
	}
}

func subscription() {
	for _, client := range clients {
		if client.subscribed {
			var book *Book

			for _, ex := range exchanges {
				if ex.GetBook().e == client.exchange {
					book = ex.GetBook()
				}
			}

			if data, ok := book.Get(client.pair); ok {
				r := ActionResponse{"subscription", data}

				if json, err := json.Marshal(r); err == nil {
					client.ch <- json
				}
			}
		}
	}
}

func pushProfit(r ProfitResponse) {
	insertQuery, err := db.Prepare("INSERT INTO deals (pair, exchanges, procent, prices, volumes) VALUES(?, ?, ?, ?, ?)")
	if err != nil {
		panic(err.Error())
	}
	defer insertQuery.Close()

	exchanges, _ := json.Marshal(r.Exchange)
	prices, _ := json.Marshal(r.Price)
	amount, _ := json.Marshal(r.Amount)

	if _, err := insertQuery.Exec(r.Pair, string(exchanges), r.Procent, string(prices), string(amount)); err != nil {
		panic(err.Error())
	}

	//tgBot.SendMessage(fmt.Sprintf(
	//	"PROFIT: %f\nPair: %v\nExchanges: %v\nPrices: %v\nAmount: %v",
	//	r.Procent, r.Pair, string(exchanges), string(prices), string(amount)))
}

func initExchanges() {
	binds, err := fetchBinds() // получаем ключи бирж
	if err != nil {
		tgBot.SendMessage(fmt.Sprintf("fetchBinds() error: %v", err.Error()))
		exchanges = []Exchange{
			Cexio{"", "", "", NewBook("cexio")},
			Bitfinex{"", "", NewBook("bitfinex")},
			Bittrex{"", "", NewBook("bittrex")},
			Exmo{"", "", NewBook("exmo")},
			Poloniex{"", "", NewBook("poloniex")},
		}
		return
	}

	exchanges = []Exchange{
		Cexio{binds["cexio"].userID, binds["cexio"].apiKey, binds["cexio"].secretKey, NewBook("cexio")},
		Bitfinex{binds["bitfinex"].apiKey, binds["bitfinex"].secretKey, NewBook("bitfinex")},
		Bittrex{binds["bittrex"].apiKey, binds["bittrex"].secretKey, NewBook("bittrex")},
		Exmo{binds["exmo"].apiKey, binds["exmo"].secretKey, NewBook("exmo")},
		Poloniex{binds["poloniex"].apiKey, binds["poloniex"].secretKey, NewBook("poloniex")},
	}
}

func fetchBinds() (map[string]Binds, error) {
	rows, err := db.Query("SELECT `exchange`, `api_key`, `secret_key`, `additional` from binds")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	bindsMap := make(map[string]Binds)
	for rows.Next() {
		var binds Binds
		err := rows.Scan(&binds.exchange, &binds.apiKey, &binds.secretKey, &binds.userID)
		if err != nil {
			return nil, err
		}
		bindsMap[binds.exchange] = binds
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return bindsMap, nil
}

func fetchTradePairs() {
	rows, err := db.Query("SELECT `name` from tradePairs")
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var newTradePairsNames []string
	newTradePairs := make(map[string]bool)

	for rows.Next() {
		var tradePairName string
		err := rows.Scan(&tradePairName)
		if err != nil {
			panic(err.Error())
		}
		newTradePairs[tradePairName] = true
		newTradePairsNames = append(newTradePairsNames, tradePairName)
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}

	tradePairs = newTradePairs
}

func fetchTradeExchanges() {
	rows, err := db.Query("SELECT `name` from tradeExchanges")
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var newTradeExchangesNames []string
	newTradeExchanges := make(map[string]Exchange)

	for rows.Next() {
		var tradeExchangeName string
		err := rows.Scan(&tradeExchangeName)
		if err != nil {
			panic(err.Error())
		}

		for _, exchange := range exchanges {
			if exchange.GetBook().e == tradeExchangeName {
				newTradeExchanges[tradeExchangeName] = exchange
				newTradeExchangesNames = append(newTradeExchangesNames, tradeExchangeName)
			}
		}
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}

	tradeExchanges = newTradeExchanges
}

func clearDB() {
	deleteQuery, err := db.Prepare("DELETE from deals")
	if err != nil {
		panic(err.Error())
	}
	defer deleteQuery.Close()

	if _, err := deleteQuery.Exec(); err != nil {
		panic(err.Error())
	}
}

func trade(r ProfitResponse) {
	var (
		exchange       []Exchange // дешевая и дорогая биржи
		balance, price []float64  // балансы бирж, цена бида и аска
		symbol         []string   // валюты в паре
	)
	exchange = append(exchange, tradeExchanges[r.Exchange[0]], tradeExchanges[r.Exchange[1]])
	symbol = append(symbol, r.Pair[:3], r.Pair[3:])
	price = append(price, r.Price[1], r.Price[3])
	// проверяем чтоб не было открытых ордеров по нашей паре
	for _, ex := range exchange {
		if orders, err := ex.OpenOrdersByPair(r.Pair); err != nil {
			panic(err.Error())
		} else if len(orders) > 0 {
			return
		}
	}
	// баланс
	for i, ex := range exchange {
		// 1-i - инверсия валют
		// на дешевой бирже проверяем баланс второй валюты (чтобы купить основную)
		// на дорогой бирже проверяем баланс основной валюты (чтобы продать)
		if bal, err := ex.Balance(symbol[1-i]); err == nil {
			if bal == 0 {
				tgBot.SendMessage(fmt.Sprintf("АХТУНГ!\nНа %v закончились %v", ex.GetBook().e, symbol[1-i]))
				return
			}
			balance = append(balance, bal*0.975) // учитываем комиссию за будущий ордер
		} else {
			tgBot.SendMessage(fmt.Sprintf("Не могу посмотреть баланс %v на %v: %v",
				symbol[1-i], ex.GetBook().e, err.Error()))
			return
		}
	}
	// определяемся с кол-вом валюты
	var avgAmount []float64
	for _, ex := range exchange {
		avgAmount = append(avgAmount, ex.GetBook().AverageAmount(r.Pair))
	}
	amount := math.Min(avgAmount[0], avgAmount[1]) // наименьшее среднее кол-во крипты по стаканам
	amount = math.Min(amount, balance[1])          // проверяем хватает ли баланса на дорогой бирже
	if price[0]*amount > balance[0] {              // проверяем хватает ли баланса на дешевой бирже
		amount = balance[0] / price[0] // ровняем кол-во крипты по наименьшему балансу
	}
	// создаем ордеры
	for i, ex := range exchange {
		var orderType string
		if i == 0 {
			orderType = "buy"
		} else {
			orderType = "sell"
		}

		order := MyOrder{"", orderType, nonce(), r.Pair, false, price[i], amount}
		if err := ex.PlaceOrder(order); err != nil {
			tgBot.SendMessage(
				fmt.Sprintf("АХТУНГ!\nОшибка при создании ордера (%v) на %v (%v): %v",
					orderType, ex.GetBook().e, r.Pair, err.Error()))
			return
		}
	}

}
