package main

import (
	"encoding/json"
	"strings"
)

func calculation(firstBook *Book, secondBook *Book) {
	for _, pair := range firstBook.GetCommonPairs(secondBook) {
		if f, errFirst := firstBook.Get(pair); errFirst {
			if s, errSecond := secondBook.Get(pair); errSecond {
				if len(f.Ask) == 0 || len(f.Bid) == 0 || len(s.Ask) == 0 || len(s.Bid) == 0 {
					continue
				}

				var r ProfitResponse
				max := 0.0
				procent := []float64{0, 0}

				if len(f.Ask) > 0 && len(s.Bid) > 0 {
					procent[0] = 100 - (100 * f.Ask[0].Price / s.Bid[0].Price) - (0.2 * 2)
				}

				if len(f.Bid) > 0 && len(s.Ask) > 0 {
					procent[1] = 100 - (100 * s.Ask[0].Price / f.Bid[0].Price) - (0.2 * 2)
				}

				if procent[0] > procent[1] {
					if max = round(procent[0]); max != 0 {
						r = ProfitResponse{
							max,
							pair,
							[]string{firstBook.e, secondBook.e},
							[]float64{f.Bid[0].Price, f.Ask[0].Price, s.Bid[0].Price, s.Ask[0].Price},
							[]float64{f.Ask[0].Amount, s.Bid[0].Amount}}
					}
				} else {
					if max = round(procent[1]); max != 0 {
						r = ProfitResponse{
							max,
							pair,
							[]string{secondBook.e, firstBook.e},
							[]float64{s.Bid[0].Price, s.Ask[0].Price, f.Bid[0].Price, f.Ask[0].Price},
							[]float64{s.Ask[0].Amount, f.Bid[0].Amount}}
					}
				}

				if max > 2.5 {
					_, ok1 := tradePairs[r.Pair]
					_, ok2 := tradeExchanges[r.Exchange[0]]
					_, ok3 := tradeExchanges[r.Exchange[1]]
					if ok1 && ok2 && ok3 {
						trade(r) // торгуем если наша пара и биржи
					}

					key := strings.Join([]string{firstBook.e, secondBook.e, pair}, "-")
					if _, ok := lastProfit[key]; !ok {
						lastProfit[key] = max
					} else {
						if lastProfit[key] != max {
							lastProfit[key] = max
						} else {
							continue
						}
					}

					pushProfit(r) // добавляем в бд если lastprofit[key] != max

					if e, err := json.Marshal(r); err == nil {
						sendToAll(e)
					} else {
						panic(err.Error())
					}
				}
			}
		}
	}
}