package main

type ApiResponse map[string]interface{}
type ApiParams map[string]string

type ProfitResponse struct {
	Procent  float64
	Pair     string
	Exchange []string
	Price    []float64
	Amount   []float64
}

type ActionResponse struct {
	Action string
	Data interface{}
}

type Exchange interface {
	Method(method string, params ApiParams) ([]byte, error)
	Balance(symbol string) (float64, error)
	PlaceOrder(order MyOrder) error
	OpenOrdersByPair(pair string) ([]interface{}, error)
	Pusher()
	GetBook() *Book
}

type client struct {
	ch chan []byte
	subscribed bool
	exchange string
	pair string
}

type Binds struct {
	exchange, apiKey, secretKey, userID string
}