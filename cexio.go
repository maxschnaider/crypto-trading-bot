package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"jsonparser"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"websocket"
)

type Cexio struct {
	userID, key, secret string
	book                *Book
}

func (s Cexio) Method(method string, params ApiParams) ([]byte, error) {
	nonce := nonce()
	payload := url.Values{}
	payload.Add("key", s.key)
	payload.Add("nonce", nonce)
	payload.Add("signature", hmacMessage("sha256", nonce + s.userID + s.key, s.secret))
	if params != nil {
		for key, value := range params {
			payload.Add(key, value)
		}
	}

	payloadStr := payload.Encode()

	req, _ := http.NewRequest("POST", "https://cex.io/api/" + method, bytes.NewBuffer([]byte(payloadStr)))
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}
	resp, err1 := client.Do(req)
	if err1 != nil {
		return nil, err1
	}
	defer resp.Body.Close()

	body, err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		return nil, err2
	}

	return body, nil
}

func (s Cexio) Balance(symbol string) (float64, error) {
	resp, err1 := s.Method("balance/", nil)
	if err1 != nil {
		return 0, err1
	}

	balanceStr, err2 := jsonparser.GetString(resp, symbol, "available")
	if err2 != nil {
		return 0, err2
	}

	balance, err3 := strconv.ParseFloat(balanceStr, 64)
	if err3 != nil {
		return 0, err3
	}

	return balance, nil
}

func (s Cexio) PlaceOrder(order MyOrder) error {
	symbol1 := order.Pair[:3]
	symbol2 := order.Pair[3:]

	params := make(map[string]string)
	params["type"] = order.Type
	params["price"] = fmt.Sprintf("%f", order.Price)
	params["amount"] = fmt.Sprintf("%f", order.Price)

	resp, err := s.Method("place_order/" + symbol1 + "/" + symbol2, params)
	if err != nil {
		return err
	}

	if complete, err := jsonparser.GetBoolean(resp, "complete"); err == nil {
		order.Complete = complete
	}
	if time, err := jsonparser.GetInt(resp, "time"); err == nil {
		order.Time = fmt.Sprintf("%d", time)
	}
	if id, err := jsonparser.GetString(resp, "id"); err == nil {
		order.ID = id
	}

	s.book.AddMyOrder(order)

	return nil
}

func (s Cexio) OpenOrdersByPair(pair string) ([]interface{}, error) {
	symbol1 := pair[:3]
	symbol2 := pair[3:]

	resp, err1 := s.Method("open_orders/" + symbol1 + "/" + symbol2, nil)
	if err1 != nil {
		return nil, err1
	}

	var orders []interface{}
	if err := json.Unmarshal(resp, &orders); err != nil {
		return nil, err
	}

	return orders, nil
}

func (s Cexio) Pusher() {
	c, _, err := websocket.DefaultDialer.Dial("wss://ws.cex.io/ws", nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	request := `{"e": "subscribe", "rooms":["pair-BTC-USD", "pair-ETH-USD", "pair-BCH-USD", "pair-BTG-USD", "pair-DASH-USD", "pair-LTC-USD", "pair-XRP-USD", "pair-XLM-USD", "pair-ZEC-USD", "pair-BTC-EUR", "pair-ETH-EUR", "pair-BCH-EUR", "pair-BTG-EUR", "pair-DASH-EUR", "pair-XRP-EUR", "pair-XLM-EUR", "pair-ZEC-EUR", "pair-BTC-GBP", "pair-ETH-GBP", "pair-BCH-GBP", "pair-DASH-GBP", "pair-ZEC-GBP", "pair-BTC-RUB", "pair-ETH-BTC", "pair-BCH-BTC", "pair-BTG-BTC", "pair-DASH-BTC", "pair-LTC-BTC", "pair-XRP-BTC", "pair-XLM-BTC", "pair-ZEC-BTC", "pair-QASH-BTC", "pair-QASH-ETH"]}`

	if err := c.WriteMessage(websocket.TextMessage, []byte(request)); err != nil {
		log.Println("send:", err)
	}

	for {
		_, json, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			return
		}

		action, e := jsonparser.GetString(json, "e")
		if e != nil || action != "md" {
			continue
		}

		pair, _ := jsonparser.GetString(json, "data", "pair")
		pair = strings.Replace(pair, ":", "", 1)
		s.book.Clear(pair)

		_, errorBid := jsonparser.ArrayEach(json, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {

			price, _ := jsonparser.GetFloat(value, "[0]")
			amount, _ := jsonparser.GetFloat(value, "[1]")

			s.book.AddBid(pair, price, amount / 100000000)
		}, "data", "buy")

		_, errorAsk := jsonparser.ArrayEach(json, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {

			price, _ := jsonparser.GetFloat(value, "[0]")
			amount, _ := jsonparser.GetFloat(value, "[1]")

			s.book.AddAsk(pair, price, amount / 100000000)
		}, "data", "sell")

		if errorBid != nil || errorAsk != nil {
			log.Printf("recv: %s", json)
		}
	}
}

func (s Cexio) GetBook() *Book {
	return s.book
}