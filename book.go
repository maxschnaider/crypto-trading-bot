package main

import (
	"errors"
	"fmt"
	"sync"
)

const bookSize = 25

type Order struct{
	Price, Amount float64
}

type MyOrder struct{
	ID, Type, Time, Pair string
	Complete bool
	Price, Amount float64
}

type BookData struct {
	Ask, Bid []Order
}

type Book struct {
	mx sync.Mutex
	e  string
	m  map[string]*BookData
	MyOrders []MyOrder
}

func NewBook(exchange string) *Book {
	return &Book{e:exchange, m: make(map[string]*BookData)}
}

func (b *Book) AddBid(pair string, price, amount float64) {
	b.mx.Lock()
	defer b.mx.Unlock()

	updated := false

	if _, ok := b.m[pair]; !ok { // если нет такой пары создаем
		b.m[pair] = &BookData{}
	}

	for i := range b.m[pair].Bid {
		if b.m[pair].Bid[i].Price == price {
			if amount != 0 {
				b.m[pair].Bid[i].Amount = amount
			} else {
				b.m[pair].Bid = append(b.m[pair].Bid[:i], b.m[pair].Bid[i+1:]...)
			}

			updated = true
			break
		}
	}

	if !updated && amount != 0 {
		b.m[pair].Bid = append(b.m[pair].Bid, Order{price, amount})

		b.m[pair].ResortBid() // сортируем при добавлении
	}
}

func (b *Book) AddAsk(pair string, price, amount float64) {
	b.mx.Lock()
	defer b.mx.Unlock()

	updated := false

	if _, ok := b.m[pair]; !ok { // если нет такой пары создаем
		b.m[pair] = &BookData{}
	}

	for i := range b.m[pair].Ask {
		if b.m[pair].Ask[i].Price == price {
			if amount != 0 {
				b.m[pair].Ask[i].Amount = amount
			} else {
				b.m[pair].Ask = append(b.m[pair].Ask[:i], b.m[pair].Ask[i+1:]...)
			}

			updated = true
			break
		}
	}

	if !updated && amount != 0 {
		b.m[pair].Ask = append(b.m[pair].Ask, Order{price, amount})
		b.m[pair].ResortAsk() // сортируем при добавлении
	}
}

func (b *Book) Get(pair string) (*BookData, bool) {
	b.mx.Lock()
	defer b.mx.Unlock()

	val, ok := b.m[pair]

	return val, ok

}

func (b *Book) GetCommonPairs(s *Book) []string {
	b.mx.Lock()
	s.mx.Lock()

	defer func() {
		b.mx.Unlock()
		s.mx.Unlock()
	}()

	var pairs []string

	for f := range b.m {
		for s := range s.m {
			if f == s { // two exchanges has common pair
				pairs = append(pairs, f)
			}
		}
	}

	return pairs
}

func (b *Book) Clear(pair string) {
	b.mx.Lock()

	if _, ok := b.m[pair]; ok {
		b.m[pair].Bid = nil
		b.m[pair].Ask = nil
	}

	b.mx.Unlock()
}

/*func (b *Book) Lock() {
	b.mx.Lock()
}

func (b *Book) Unlock() {
	b.mx.Unlock()
}*/

func (b *BookData) ResortBid() {
	for i := range b.Bid {
		for j := range b.Bid {
			if b.Bid[j].Price < b.Bid[i].Price {
				b.Bid[j], b.Bid[i] = b.Bid[i], b.Bid[j]
			}
		}
	}

	if len(b.Bid) >= bookSize {
		b.Bid = b.Bid[:bookSize]
	}
}

func (b *BookData) ResortAsk() {
	for i := range b.Ask {
		for j := range b.Ask {
			if b.Ask[j].Price > b.Ask[i].Price {
				b.Ask[j], b.Ask[i] = b.Ask[i], b.Ask[j]
			}
		}
	}

	if len(b.Ask) >= bookSize {
		b.Ask = b.Ask[:bookSize]
	}
}

func (b *Book) GetPairs() []string {
	b.mx.Lock()

	defer func() {
		b.mx.Unlock()
	}()

	var pairs []string

	for f := range b.m {
		pairs = append(pairs, f)
	}

	return pairs
}

func (b *Book) AddMyOrder(order MyOrder) {
	b.mx.Lock()
	defer b.mx.Unlock()

	b.MyOrders = append(b.MyOrders, order)
}

func (b *Book) AverageAmount(pair string) float64 {
	b.mx.Lock()
	defer b.mx.Unlock()

	bookData, ok := b.Get(pair)
	if !ok {
		panic(errors.New(fmt.Sprintf("can not find pair %v", pair)))
	}

	sum := 0.0
	n := 0
	for _, bid := range bookData.Bid[:10] {
		sum += bid.Amount
		n += 1
	}
	for _, ask := range bookData.Ask[:10] {
		sum += ask.Amount
		n += 1
	}

	return sum / float64(n)
}