package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"jsonparser"
	"log"
	"net/http"
	"strconv"
	"strings"
	"websocket"
)

type Bitfinex struct {
	key, secret string
	book *Book
}

func (s Bitfinex) Method(method string, params ApiParams) ([]byte, error) {
	payload := make(map[string]interface{})
	payload["request"] = "/v1/" + method
	payload["nonce"] = nonce()
	if params != nil {
		for key, value := range params {
			payload[key] = value
		}
	}
	payloadJson, _ := json.Marshal(payload)
	payloadEnc := base64.StdEncoding.EncodeToString(payloadJson)

	sign := hmacMessage("sha384", payloadEnc, s.secret)

	req, _ := http.NewRequest("POST", "https://api.bitfinex.com/v1/" + method, nil)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-BFX-APIKEY", s.key)
	req.Header.Add("X-BFX-PAYLOAD", payloadEnc)
	req.Header.Add("X-BFX-SIGNATURE", sign)

	client := &http.Client{}
	resp, err1 := client.Do(req)
	if err1 != nil {
		return nil, err1
	}
	defer resp.Body.Close()

	body, err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		return nil, err2
	}

	return body, nil
}

func (s Bitfinex) Balance(symbol string) (float64, error) {
	resp, err1 := s.Method("balances", nil)
	if err1 != nil {
		return 0, err1
	}

	var balanceData []byte
	if _, err2 := jsonparser.ArrayEach(resp, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		if currency, err := jsonparser.GetString(value, "currency");
		err == nil && currency == strings.ToLower(symbol) {
			bal, _, _, _ := jsonparser.Get(value, "available")
			balanceData = bal
			return
		}
	}); err2 != nil {
		return 0, err2
	}

	balance, err3 := strconv.ParseFloat(string(balanceData), 64)
	if err3 != nil {
		return 0, err3
	}

	return balance, nil
}

func (s Bitfinex) PlaceOrder(order MyOrder) error {
	params := make(map[string]string)
	params["symbol"] = order.Pair
	params["price"] = fmt.Sprintf("%f", order.Price)
	params["amount"] = fmt.Sprintf("%f", order.Amount)
	params["exchange"] = "bitfinex"
	params["side"] = order.Type
	params["type"] = "exchange market" // market - best price to execute immediately / limit - fix price to wait for

	resp, err := s.Method("order/new", params)
	if err != nil {
		return err
	}

	if time, err := jsonparser.GetString(resp, "timestamp"); err == nil {
		order.Time = time
	}
	if id, err := jsonparser.GetInt(resp, "order_id"); err == nil {
		order.ID = fmt.Sprintf("%d", id)
	}

	s.book.AddMyOrder(order)
	return nil
}

func (s Bitfinex) OpenOrdersByPair(pair string) ([]interface{}, error) {
	resp, err := s.Method("orders", nil)
	if err != nil {
		return nil, err
	}

	var orders []interface{}
	if _, err := jsonparser.ArrayEach(resp, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		if val, _ := jsonparser.GetString(value, "symbol"); val == strings.ToLower(pair) {
			orders = append(orders, value)
		}
	}); err != nil {
		return nil, err
	}

	return orders, nil
}

func (s Bitfinex) Pusher() {
	c, _, err := websocket.DefaultDialer.Dial("wss://api.bitfinex.com/ws/2", nil)

	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	currencies := []string{"BTCUSD", "ETHUSD", "EOSUSD", "XRPUSD", "LTCUSD", "NEOUSD", "ETCUSD", "TRXUSD", "XMRUSD", "ZECUSD", "BTGUSD", "ZRXUSD", "XLMUSD", "GNTUSD", "BTCEUR", "ETHEUR", "EOSEUR", "TRXEUR", "NEOEUR", "XLMEUR", "BTCGBP", "ETHGBP", "EOSGBP", "NEOGBP", "TRXGBP", "XLMGBP", "BTCJPY", "ETHJPY", "EOSJPY", "TRXJPY", "NEOJPY", "XLMJPY", "ETHBTC", "EOSBTC", "XRPBTC", "LTCBTC", "ETCBTC", "XMRBTC", "NEOBTC", "ZECBTC", "BTGBTC", "TRXBTC", "ZRXBTC", "XLMBTC", "EOSETH", "NEOETH", "ZRXETH", "TRXETH"}
	pairs := map[int]string{}

	for _, v := range currencies {
		response := fmt.Sprintf(`{"event":"subscribe", "channel":"book", "symbol":"%s"}`, v)

		if err := c.WriteMessage(websocket.TextMessage, []byte(response)); err != nil {
			log.Println("send:", err)
		}
	}

	for {
		_, json, err := c.ReadMessage()

		if err != nil {
			log.Println("read:", err)
			return
		}

		if len(currencies) != len(pairs) { // waiting for filling all channels ID
			event, eventErr := jsonparser.GetString(json, "event")

			if eventErr != nil {
				continue
			}

			if event == "subscribed" {
				chanId, _ := jsonparser.GetInt(json, "chanId")
				pair, _ := jsonparser.GetString(json, "pair")

				pairs[int(chanId)] = pair

				continue
			}
		}

		chanId, _ := jsonparser.GetInt(json, "[0]")
		price, errPrice := jsonparser.GetFloat(json, "[1]", "[0]")
		count, _ := jsonparser.GetFloat(json, "[1]", "[1]")
		amount, _ := jsonparser.GetFloat(json, "[1]", "[2]")
		pair := pairs[int(chanId)]

		if errPrice != nil {
			//log.Println("read:", errPrice, string(json))

			continue
		}

		if amount > 0 {
			if count != 0 {
				s.book.AddBid(pair, price, amount)
			} else {
				s.book.AddBid(pair, price, 0)
			}
		} else {
			if count != 0 {
				s.book.AddAsk(pair, price, -(amount))
			} else {
				s.book.AddAsk(pair, price, 0)
			}
		}

		//log.Printf("recv: %s", json)
	}
}

func (s Bitfinex) GetBook() *Book {
	return s.book
}