package main

/*
 * Bot taking information about order books of many markets
 * some in real-time and some onesecond-time update
 * also multithreaded and fast
 * Config file: config.json
 * Created by @maxschnaider
 */

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"os"
	"time"
)

const version = "1.06b"
const timeStart = 10
const tgApiKey = "746504492:AAGpyT5rtyodMfjFP0yqAzbveSciwdrhBdk"
const tgChatID = "-323938243"

var (
	upgrader       websocket.Upgrader
	clients        map[*websocket.Conn]*client
	db             *sql.DB
	exchanges      []Exchange          // биржи
	lastProfit     map[string]float64  // последний максимальный профит по конкретной бирже и паре
	tradePairs     map[string]bool     // пары которыми торгуем
	tradeExchanges map[string]Exchange // биржи на которых торгуем
	tgBot          TGBot
)

func main() {
	clients = make(map[*websocket.Conn]*client)
	lastProfit = make(map[string]float64)
	tradePairs = make(map[string]bool)
	tradeExchanges = make(map[string]Exchange)
	tgBot = TGBot{tgApiKey, tgChatID}
	upgrader = websocket.Upgrader{}
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	conf, err := loadSettings("config.json")
	fmt.Println("OrderBot v" + version + " starting..")
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("Config loaded succesfully.")
		db, _ = sql.Open("mysql", conf["db_user"]+":"+conf["db_pass"]+"@/"+conf["db_base"])

		if db.Ping() != nil {
			fmt.Println("Connecting to database '" + conf["db_base"] + "' failed.")
			os.Exit(0)
		} else {
			fmt.Println("Connecting to database '" + conf["db_base"] + "' succesfully.\n")
		}

		defer func() {
			if err := db.Close(); err != nil {
				panic(err.Error())
			}
		}()
	}

	initExchanges()
	fmt.Println("Total markets:", len(exchanges))

	for _, ex := range exchanges {
		go ex.Pusher() // цикл мониторинга стаканов
	}

	fmt.Printf("Waiting for filling Order Book Data.. %ds\n", timeStart)
	time.Sleep(timeStart * time.Second)
	fmt.Println("Great! Successful start.")

	go loop()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		c, _ := upgrader.Upgrade(w, r, nil)
		clients[c] = &client{make(chan []byte), false, "", ""}

		go pusher(c)
		go reader(c)
	})
	//log.Fatal(http.ListenAndServe(":8100", nil))
	log.Fatal(http.ListenAndServeTLS(":8100", conf["cert"], conf["cert_key"], nil))
}

func loop() {
	sec := 0
	for {
		for i, first := range exchanges {
			firstBook := first.GetBook()

			for j, second := range exchanges {
				if j <= i {
					continue
				}

				calculation(firstBook, second.GetBook()) // расчет профита между разными биржами
			}
		}

		subscription()

		if sec%300 == 0 {
			fetchTradePairs() // каждые 5 мин обновляем пары и биржи для торговли
			fetchTradeExchanges()
		}
		if sec >= 3600 {
			clearDB() // каждый час чистим бд
			sec = 0
		}
		sec += 1
		time.Sleep(1 * time.Second)
	}
}
