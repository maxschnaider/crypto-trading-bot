package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"jsonparser"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"websocket"
)

type Bittrex struct {
	key, secret string
	book *Book
}

func (s Bittrex) Method(method string, params ApiParams) ([]byte, error) {
	baseURL := "https://api.bittrex.com/api/v1.1/"
	payload := url.Values{}
	payload.Add("nonce", nonce())
	payload.Add("apikey", s.key)
	if params != nil {
		for key, value := range params {
			payload.Add(key, value)
		}
	}

	payloadSrt := payload.Encode()
	requestURL := baseURL + method + "?" + payloadSrt
	sign := hmacMessage("sha512", requestURL, s.secret)

	req, _ := http.NewRequest("POST", requestURL, nil)
	req.Header.Set("apisign", sign)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36")
	req.Header.Add("Content-Length", strconv.Itoa(len(payloadSrt)))

	client := &http.Client{}
	resp, err1 := client.Do(req)
	if err1 != nil {
		return nil, err1
	}
	defer resp.Body.Close()

	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		return nil, err1
	}

	return body, nil
}

func (s Bittrex) Balance(symbol string) (float64, error) {
	params := make(map[string]string)
	params["currency"] = symbol

	resp, err1 := s.Method("account/getbalance", params)
	if err1 != nil {
		return 0, err1
	}

	balance, err2 := jsonparser.GetFloat(resp, "result", "Available")
	if err2 != nil {
		return 0, err2
	}

	return balance, nil
}

func (s Bittrex) PlaceOrder(order MyOrder) error {
	pair := order.Pair[3:] + "-" + order.Pair[:3] // в bittrex пары перевернуты
	params := make(map[string]string)
	params["market"] = pair
	params["rate"] = fmt.Sprintf("%f", order.Price)
	params["quantity"] = fmt.Sprintf("%f", order.Amount)

	resp, err := s.Method("market/" + order.Type + "limit/", params)
	if err != nil {
		return err
	}

	order.Complete = true
	if id, err := jsonparser.GetString(resp, "result", "uuid"); err == nil {
		order.ID = id
	}

	s.book.AddMyOrder(order)
	return nil
}

func (s Bittrex) OpenOrdersByPair(pair string) ([]interface{}, error) {
	pair = pair[3:] + "-" + pair[:3] // в bittrex пары перевернуты
	params := make(map[string]string)
	params["market"] = pair

	resp, err := s.Method("market/getopenorders", params)
	if err != nil {
		return nil, err
	}

	var orders []interface{}
	resultData, _, _, _ := jsonparser.Get(resp, "result")
	if err := json.Unmarshal(resultData, &orders); err != nil {
		return nil, err
	}

	return orders, nil
}

func (s Bittrex) Pusher() {
	c, _, err := websocket.DefaultDialer.Dial("wss://socket.bittrex.com/signalr/connect?transport=webSockets&clientProtocol=1.5&connectionToken=nkBZjTrF0JZhsO8ATgQF7t6YJxWhgfCLMuVpKBG7fkDIgngUrPnbZLKlrbZoXpxdsph86%2BD5MPhGeEM%2FUhR1Q4GuNbZpdh9lmObBNGD0CaaB9XMm&connectionData=%5B%7B%22name%22%3A%22c2%22%7D%5D", nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	currencies := []string{"USD-BTC", "USD-XRP", "USD-ETH", "USD-BCH", "USD-ADA", "USD-TRX", "USD-LTC", "USD-ETC", "USD-ZEC", "USD-ZRX", "BTC-WAVES", "BTC-BCH", "BTC-XRP", "BTC-ETH", "BTC-BSV", "BTC-PMA", "BTC-XLM", "BTC-ADA", "BTC-LTC", "BTC-DGB", "BTC-XVG", "BTC-LSK", "BTC-DOGE", "BTC-TRX", "BTC-OCN", "BTC-DASH", "BTC-ETC", "BTC-XMR", "BTC-NEO", "BTC-ZEC", "BTC-ZRX", "BTC-NXT", "ETH-BCH", "ETH-WAVES", "ETH-XRP", "ETH-LTC", "ETH-TRX", "ETH-ADA", "ETH-XLM", "ETH-NEO", "ETH-DGB", "ETH-XMR", "ETH-DASH", "ETH-OCN", "ETH-ZEC", "ETH-ZRX"}

	for _, v := range currencies {
		response := fmt.Sprintf(`{"H":"c2","M":"SubscribeToExchangeDeltas","A":["%s"],"I":1}`, v)

		if err := c.WriteMessage(websocket.TextMessage, []byte(response)); err != nil {
			log.Println("send:", err)

			return
		}
	}

	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)

			return
		}

		val, e := jsonparser.GetString(message, "M", "[0]", "A", "[0]")
		if e != nil {
			continue
		}

		json := inflate(val)
		pair, _ := jsonparser.GetString(json, "M")

		re := regexp.MustCompile("([A-z]+)-([A-z]+)")
		pair = re.ReplaceAllString(pair, "$2$1")

		_, errorBid := jsonparser.ArrayEach(json, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
			price, _ := jsonparser.GetFloat(value, "R")
			amount, _ := jsonparser.GetFloat(value, "Q")

			s.book.AddBid(pair, price, amount)
		}, "Z")

		_, errorAsk := jsonparser.ArrayEach(json, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {

			price, _ := jsonparser.GetFloat(value, "R")
			amount, _ := jsonparser.GetFloat(value, "Q")

			s.book.AddAsk(pair, price, amount)
		}, "S")

		if errorBid != nil && errorAsk != nil {
			log.Printf("recv: %s", json)
		}
	}
}

func (s Bittrex) GetBook() *Book {
	return s.book
}