package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"jsonparser"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type Exmo struct {
	key, secret string
	book *Book
}

func (s Exmo) Method(method string, params ApiParams) ([]byte, error) {
	payload := url.Values{}
	payload.Add("nonce", nonce())
	if params != nil {
		for key, value := range params {
			payload.Add(key, value)
		}
	}
	payloadSrt := payload.Encode()

	sign := hmacMessage("sha512", payloadSrt, s.secret)

	req, _ := http.NewRequest("POST", "https://api.exmo.com/v1/"+method, bytes.NewBuffer([]byte(payloadSrt)))
	req.Header.Set("Key", s.key)
	req.Header.Set("Sign", sign)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36")
	req.Header.Add("Content-Length", strconv.Itoa(len(payloadSrt)))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		return nil, err2
	}

	return body, nil
}

func (s Exmo) Balance(symbol string) (float64, error) {
	resp, err1 := s.Method("user_info", nil)
	if err1 != nil {
		return 0, err1
	}

	balanceData, _, _, err2 := jsonparser.Get(resp, "balances", symbol)
	if err2 != nil {
		return 0, err2
	}

	balance, err3 := strconv.ParseFloat(string(balanceData), 64)
	if err3 != nil {
		return 0, err3
	}

	return balance, nil
}

func (s Exmo) PlaceOrder(order MyOrder) error {
	symbol1 := order.Pair[:3]
	symbol2 := order.Pair[3:]

	params := make(map[string]string)
	params["type"] = order.Type
	params["price"] = fmt.Sprintf("%f", order.Price)
	params["amount"] = fmt.Sprintf("%f", order.Price)

	resp, err := s.Method("place_order/" + symbol1 + "/" + symbol2, params)
	if err != nil {
		return err
	}

	order.Time = nonce()
	if id, err := jsonparser.GetInt(resp, "order_id"); err == nil {
		order.ID = fmt.Sprintf("%d", id)
	}

	s.book.AddMyOrder(order)
	return nil
}

func (s Exmo) OpenOrdersByPair(pair string) ([]interface{}, error) {
	pair = pair[:3] + "_" + pair[3:]

	resp, err1 := s.Method("user_open_orders/", nil)
	if err1 != nil {
		return nil, err1
	}

	ordersData, _, _, err2 := jsonparser.Get(resp, pair)
	if err2 != nil {
		return nil, nil // no active orders for current pair
	}

	var orders []interface{}
	if err := json.Unmarshal(ordersData, &orders); err != nil {
		return nil, err
	}

	return orders, nil
}

func (s Exmo) Pusher() {
	currencies := []string{"BTC_USD", "BTC_EUR", "BTC_RUB", "LSK_BTC", "NEO_BTC", "ZRX_BTC", "GNT_BTC", "TRX_BTC", "XLM_BTC", "EOS_BTC", "BTG_BTC", "BCH_BTC", "DASH_BTC", "ETH_BTC", "ETC_BTC", "LTC_BTC", "ZEC_BTC", "XRP_BTC", "XMR_BTC", "DOGE_BTC", "WAVES_BTC", "ZRX_ETH", "GNT_ETH", "BCH_ETH", "ETH_LTC", "ETH_USD", "XRP_ETH", "XMR_ETH", "XRP_USD", "XRP_RUB", "XRP_EUR", "LTC_USD", "LTC_EUR", "LSK_USD", "NEO_USD", "TRX_USD", "XLM_USD", "EOS_USD", "BCH_USD", "DASH_USD", "ZEC_USD", "XMR_USD"}

	for {
		get("https://api.exmo.com/v1/order_book/?pair=" + strings.Join(currencies, ",") + "&limit=25", func(response []byte) {
			err := jsonparser.ObjectEach(response, func(key []byte, value []byte, dataaType jsonparser.ValueType, offset int) error {
				pair := strings.Replace(string(key), "_", "", 1)

				s.book.Clear(pair)

				_, errAsk := jsonparser.ArrayEach(value, func(value []byte, dataaType jsonparser.ValueType, offset int, err error) {
					p, _ := jsonparser.GetString(value, "[0]")
					a, _ := jsonparser.GetString(value, "[1]")
					price, _ := strconv.ParseFloat(p, 64)
					amount, _ := strconv.ParseFloat(a, 64)

					s.book.AddAsk(pair, price, amount)
				}, "ask")

				_, errBid := jsonparser.ArrayEach(value, func(value []byte, dataaType jsonparser.ValueType, offset int, err error) {
					p, _ := jsonparser.GetString(value, "[0]")
					a, _ := jsonparser.GetString(value, "[1]")
					price, _ := strconv.ParseFloat(p, 64)
					amount, _ := strconv.ParseFloat(a, 64)

					s.book.AddBid(pair, price, amount)
				}, "bid")

				if errAsk != nil || errBid != nil {
					log.Printf("send: %s %s", errAsk, errBid)
				}

				return nil
			})

			if err != nil {
				log.Printf("send: %s", err)
			}
		})

		time.Sleep(1 * time.Second)
	}
}

func (s Exmo) GetBook() *Book {
	return s.book
}